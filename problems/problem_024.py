# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    return sum(values)/len(values)

values = [3, 5, 6, 7]
average = calculate_average(values)

print(calculate_average(values))
